# Articles worth reading

* Journeys https://about.gitlab.com/handbook/journeys
* I can't find it. There was a great UX article about different ways to handle user mistakes (e.g. confirmation prompts, undo, etc.)
* close, but not quite right:
    * https://uxdesign.cc/are-you-sure-you-want-to-do-this-microcopy-for-confirmation-dialogues-1d94a0f73ac6
    * https://uxdesign.cc/delete-models-in-products-because-sometimes-cats-walk-on-keyboards-6f886b767f6d

considerations:
* users delete things when they did not mean to
* users delete things other then the ones they intended to
* users sometimes intentionally delete and regret later
* adding friction to *frequent* interactions is undesirable
* some mistakes are more costly than others
* different approaches have different implementation cost

possible UI:
* nothing: no confirmaton, no undo
* confirmation prompt (subtopic: making a good confirmation prompt)
* misclick-resistent interaction (press and hold; swipe; drag; unlock deletion)
* undo
* trash can
