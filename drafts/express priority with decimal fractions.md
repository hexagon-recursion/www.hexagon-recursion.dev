example:
* 0.01
* 0.014
* 0.0145
* 0.015
* 0.025
* 0.05
* 0.1
* 0.2
* 0.25
* 0.26
* 0.3
* 0.4

- Allowed range 0 < p < 1
- Ideally infinite presision (use str or x*10^y instead of double; fixed y is not too bad either)
- Optional: disallow repeats to avoid ambiguity

This allows insertion at any position
