# Book recommendations
* [Structure and Interpretation of Computer Programs aka SICP](https://mitpress.mit.edu/sites/default/files/sicp/index.html)

## Programming languages
* [C: The C Programming Language by Brian Kernighan and Dennis Ritchie](https://en.wikipedia.org/wiki/The_C_Programming_Language)
* [Pyhton: Think Python 2e](https://greenteapress.com/wp/think-python-2e/)
* [Haskell: Learn You a Haskell for Great Good!](http://learnyouahaskell.com/)
* [OCaml: Real World OCaml (Functional programming for the masses) 2nd Edition](https://dev.realworldocaml.org/)
* Scheme: ?
* Clojure: ?
* JavaScript: ?
* SQL: ?
* Rust: ?
* Go: ?
