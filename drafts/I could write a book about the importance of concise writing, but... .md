# I could write a book about the importance of concise writing, but...

Emphathise the reader. She is busy. He has a finite lifespan and a virtually infinite number of things she wants to do.

## This is _usually_ taking it too far:

concise writing = important
Emphathise reader: busy, finite lifespan, virtually infinite number of ideas&desires
