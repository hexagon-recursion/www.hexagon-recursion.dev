* look and feel
* accessibility
* platforms
    * desktop
        * linux
        * windows
        * mac
        * more?
    * mobile
        * android
        * ios
        * more?
    * web
    * more?
* deployment story
* developer amenities
    * repl
    * hot reload
    * testing
    * more?
* project
    * license
    * what makes me special?
    * goals
    * non-goals
    * maturity
    * health
    * ecosystem
        * users
        * bindings
        * 3rd party value-adds (such as widget libraries)
* language
    * inclusion criteria:
        * The categories are **not** mutually exclusive
        * category: homoiconic
            * "macros": a convinient way to extend the notation; enable things that subroutines can't
            * embedded domiain-specific languages
        * category: good static typing
            * sum types
            * pattern matching
            * type inference
            * some form of generics
            * typeclasses or traits or first class modules or ... (i.e. some obvious and convinient solution to the expression problem)
                * https://en.wikipedia.org/wiki/Expression_problem
                * https://eli.thegreenplace.net/2016/the-expression-problem-and-its-solutions/
                * https://citeseerx.ist.psu.edu/viewdoc/download?doi=10.1.1.119.5187&rep=rep1&type=pdf
                * https://githubhelp.com/tmountain/expression-problem
    * license
    * what makes me special?
    * goals
    * non-goals
    * maturity
    * health
    * native ecosystem
    * interop
